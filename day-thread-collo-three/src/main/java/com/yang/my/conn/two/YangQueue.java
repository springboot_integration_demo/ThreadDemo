package com.yang.my.conn.two;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Queue
 * @Author: Mu_YI
 * @Date: 2019/3/20 20:34
 * @Version 1.0
 */
public class YangQueue {

    public static void main(String[] args) {

        ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
        concurrentLinkedQueue.offer("a");
        concurrentLinkedQueue.offer("b");
        concurrentLinkedQueue.offer("c");
        concurrentLinkedQueue.offer("d");
        concurrentLinkedQueue.offer("e");

        System.out.println(concurrentLinkedQueue);
        // 从头部取出元素;并从列队中取出
        System.out.println(concurrentLinkedQueue.poll());
        System.out.println(concurrentLinkedQueue.size());
        // 去获取值,并不出压出去
        System.out.println(concurrentLinkedQueue.peek());
        System.out.println("=================");

        ArrayBlockingQueue<String> a = new ArrayBlockingQueue<String>(5);
        a.add("a");
        a.add("b");
        a.add("c");
        a.add("d");
        a.add("e");
        System.out.println(a);

        // 阻塞队列
        LinkedBlockingDeque<String> l = new LinkedBlockingDeque();
        l.offer("a");
        l.offer("b");
        l.offer("c");
        l.offer("d");
        l.offer("e");
        System.out.println(l.size());

        for(Iterator iterator =l.iterator();iterator.hasNext();){
            String str = (String) iterator.next();
            System.out.println(str);
        }


    }

}
