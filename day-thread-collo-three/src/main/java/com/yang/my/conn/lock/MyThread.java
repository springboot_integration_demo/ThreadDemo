package com.yang.my.conn.lock;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 17:01
 * @Version 1.0
 */
public class MyThread extends Thread {

    private MyService myService;

    public MyThread(MyService myService){
        this.myService = myService;
    }

    @Override
    public void run() {
        myService.test();
    }

}
