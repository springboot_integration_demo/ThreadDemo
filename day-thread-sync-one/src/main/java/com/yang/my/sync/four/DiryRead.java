package com.yang.my.sync.four;

import java.util.Collections;

/**
 *业务整理需要使用完整的synchronized,保持业务的原子性
 * @Author: Mu_YI
 * @Date: 2019/3/17 20:59
 * @Version 1.0
 */
public class DiryRead {

    private String username = "yang";
    private String password = "123";

    private synchronized void setValue(String username,String password){
        this.username = username;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.password = password;
        System.out.println("SetValue的方法得到:username:" + this.username + ":password:" + this.password);
    }

    private void getValue(){
        System.out.println("GetValue的方法得到:username:" + this.username +";password:" + this.password);
    }

    public static void main(String[] args) throws Exception {
        DiryRead diryRead = new DiryRead();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                diryRead.setValue("hhhh","11111");
            }
        });
        t1.start();
        Thread.sleep(1000);
        diryRead.getValue();
    }
}
