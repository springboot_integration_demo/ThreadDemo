package com.yang.my.conn.two;

import java.util.concurrent.LinkedBlockingDeque;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:49
 * @Version 1.0
 */
public class YangUseLinkedBlockingDeque {

    public static void main(String[] args) {
        LinkedBlockingDeque<String> dq = new LinkedBlockingDeque<>();

        dq.addFirst("a");
        dq.addFirst("b");
        dq.addFirst("c");
        dq.addFirst("d");
        dq.addFirst("e");
        dq.addLast("f");
        dq.addLast("g");
        dq.addLast("h");
        dq.addLast("i");
        dq.addLast("j");

        System.out.println("查看头元素:"+dq.peekFirst());
        System.out.println("查看尾元素:"+dq.peekLast());
        Object[] objects = dq.toArray();
        for(int i=0;i<objects.length;i++){
            System.out.println(objects[i]);
        }
        System.out.println(dq);

    }

}
