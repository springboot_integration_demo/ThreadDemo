package com.yang.my.task;

import java.util.Random;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:35
 * @Version 1.0
 */
public class AppMain {

    public static void main(String[] args) {

        Master master = new Master(new Worker(),20);

        Random random = new Random();
        for(int i=1;i<=100;i++){
            Task t = new Task();
            t.setId(i);
            t.setPrice(random.nextInt(1000));
            master.submit(t);
        }

        master.execute();
        long start = System.currentTimeMillis();

        while (true){
            if(master.isComplete()){
                long end = System.currentTimeMillis();
                int priceResult = master.getResult();
                System.out.println("最终的结果:" + priceResult + ";执行时间:" + end);
                break;
            }
        }
    }

}
