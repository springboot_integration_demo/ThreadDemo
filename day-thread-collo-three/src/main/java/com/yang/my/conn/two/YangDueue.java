package com.yang.my.conn.two;

import java.util.concurrent.LinkedBlockingDeque;

/**
 * Dueue
 * @Author: Mu_YI
 * @Date: 2019/3/20 20:47
 * @Version 1.0
 */
public class YangDueue {

    public static void main(String[] args) {
        LinkedBlockingDeque<String> dq = new LinkedBlockingDeque<String>(10);
        dq.addFirst("a");
        dq.addFirst("b");
        dq.addFirst("c");
        dq.addFirst("d");
        dq.addFirst("e");
        dq.addFirst("f");
        dq.addFirst("g");
        dq.addFirst("h");
        dq.addFirst("i");
        dq.addFirst("j");
        // dq.addFirst("k");
        System.out.println(dq.size());
        System.out.println("头部第一个元素:" + dq.pollFirst());
        System.out.println("尾部最后一个元素:" + dq.pollLast());
        Object[] objs = dq.toArray();
        for(int i=0;i<objs.length;i++){
            System.out.println(objs[i]);
        }
    }
}
