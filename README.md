##               JAVA线程的学习

这里记录下多线程的学习代码

1.   day-thread-sync-one 项目中

    

   | com.yang.my.sync.one.MyThread        | 继承一个Thread，顶一个全局变量cout,在run中--;然后开启多个线程跑 |
   | ------------------------------------ | :----------------------------------------------------------- |
   | com.yang.my.sync.two.MyThread        | 关键字synchronized取得的锁都是对象锁;而不是一段代码(方法)当作锁.那个线程先执行synchronized关键字,那个线程就持有该方法所属对象的锁(LOCK) |
   | com.yang.my.sync.three.ObjectThread  | t1线程先持有object对象的Lock锁，t2线程可以以异步的方式调用对象中的非synchronized修饰的方法.t1线程先持有object对象的Lock锁，t2线程如果在这个时候调用对象中的同步（synchronized）方法则需等待，也就是同步 |
   | com.yang.my.sync.four.DiryRead       | 务整理需要使用完整的synchronized,保持业务的原子性            |
   | com.yang.my.sync.five.SyncReentryOne | synchronized重入                                             |
   | com.yang.my.sync.six.StringLock      | synchronized代码块对字符串的锁,注意String常量池的缓存功能    |
   | com.yang.my.sync.six.ObjectLock      | 对象锁                                                       |
   | com.yang.my.sync.six.DeadLock        | 模拟死锁;在设计程序时就应该避免双方相互持有对方的锁的情况    |

2. day-thread-conn-two 项目中

    

   | com.yang.my.coon.one.ListAddOne        | volatile的字段时使用.使用List集合来添加元素 |
   | -------------------------------------- | ------------------------------------------- |
   | com.yang.my.coon.one.ListAddTwo        | wait notifiy方法; wait释放锁,notfiy不释放锁 |
   | com.yang.my.coon.two.MyQueue           | 模拟queue(队列)                             |
   | com.yang.my.coon.three.ConnThreadLocal | ThreadLocal的使用                           |
   | com.yang.my.coon.five.Signletion       | 单例模式的饿汉式的实现                      |
   | com.yang.my.coon.five.DoubleSingleton  | 单利模式二个if加synchronized实现            |
   |                                        |                                             |
   |                                        |                                             |

3. day-thread-colle-three 项目中

    

   | com.yang.my.conn.one.Tickets | 模拟经典案列买火车票 |
   | ---------------------------- | -------------------- |
   |                              |                      |
   |                              |                      |
   |                              |                      |
   |                              |                      |
   |                              |                      |
   |                              |                      |
   |                              |                      |

4. day-thread-sync-four

   | com.yang.my.future 包下 | 模拟future的实现 |
   | ----------------------- | ---------------- |
   | com.yang.my.task 包下   | 模拟Task任务执行 |
   |                         |                  |
   |                         |                  |
   |                         |                  |
   |                         |                  |
   |                         |                  |
   |                         |                  |

后续个人有时间学习的话，是会一直更新的，也欢迎各位提出意见。目前包名;类名字取到不是很好。后续会进行修改的。

