package com.yang.my.task;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:15
 * @Version 1.0
 */
public class Master {

    //1 有一个盛放任务的容器
    private ConcurrentLinkedQueue<Task> workQueue = new ConcurrentLinkedQueue<Task>();

    //2 需要有一个盛放worker的集合
    private HashMap<String,Thread> worker = new HashMap<>();

    //3 需要有一个盛放每一个worker执行任务的结果集合
    private ConcurrentHashMap<String,Object> resultMap = new ConcurrentHashMap<>();

    //4 构造方法
    public Master(Worker worker,int workCount){
        worker.setWorkQueue(workQueue);
        worker.setResultMap(resultMap);
        for(int i=0;i<workCount;i++){
            this.worker.put(Integer.toString(i),new Thread(worker));
        }
    }

    //5 需要一个提交任务的方法
    public void submit(Task task){
        this.workQueue.add(task);
    }

    //6 需要有一个执行的方法，启动所有的worker方法去执行任务
    public void execute(){
        for(Map.Entry<String,Thread> me:worker.entrySet()){
            me.getValue().start();
        }
    }

    //7 判断是否运行结束的方法
    public boolean isComplete(){
        for(Map.Entry<String,Thread> me:worker.entrySet()){
            if(me.getValue().getState() != Thread.State.TERMINATED){
                return false;
            }
        }
        return true;
    }

    //8 计算结果方法
    public int getResult(){
        int priceResult = 0;
        for(Map.Entry<String,Object> me:resultMap.entrySet()){
            priceResult += (Integer) me.getValue();
        }
        return priceResult;
    }


}
