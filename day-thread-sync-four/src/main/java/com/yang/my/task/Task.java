package com.yang.my.task;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:16
 * @Version 1.0
 */
public class Task {

    private int id;
    private int price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
