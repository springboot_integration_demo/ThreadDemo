package com.yang.my.sync.seven;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Volatile不具有synchronized的原子性
 * @Author: Mu_YI
 * @Date: 2019/3/18 22:57
 * @Version 1.0
 */
public class VolatileNoAtomic extends  Thread{

    private static AtomicInteger count = new AtomicInteger(0);

    private static void addCount(){
        for(int i=0;1<1000;i++){
            count.incrementAndGet();
        }
    }

    @Override
    public void run() {
        addCount();
    }

    public static void main(String[] args) {
        VolatileNoAtomic [] arr = new VolatileNoAtomic[100];
        for(int i=0;i<10;i++){
            arr[i] = new VolatileNoAtomic();
        }

        for(int i=0;i<10;i++){
            arr[i].start();
        }
        System.out.println(count);
    }
}
