package com.yang.my.future;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:01
 * @Version 1.0
 */
public class FutureData implements Data {

    private RealData realData;

    private boolean isRead = false;

    public synchronized void setRealData(RealData realData){
        if(isRead){
            return;
        }
        this.realData = realData;
        isRead = true;
        // 进行通知
        notify();
    }

    @Override
    public synchronized String getRequest() {
        while(!isRead){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return this.realData.getRequest();
    }
}
