package com.yang.my.sync.six;

/**
 * 锁对象改变的问题
 *
 * @Author: Mu_YI
 * @Date: 2019/3/17 21:34
 * @Version 1.0
 */
public class ChangeLock {

    private String lock = "lock";

    private void method(){
        synchronized (lock){
            try {
                System.out.println("当前线程:" + Thread.currentThread().getName()+"开始");
                lock = "changeLock";
                Thread.sleep(2000);
                System.out.println("当前线程:" + Thread.currentThread().getName()+"结束");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ChangeLock changeLock = new ChangeLock();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                changeLock.method();
            }
        },"t1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                changeLock.method();
            }
        },"t2");

        t1.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
    }

}
