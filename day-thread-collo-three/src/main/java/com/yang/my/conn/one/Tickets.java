package com.yang.my.conn.one;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:37
 * @Version 1.0
 */
public class Tickets {

    public static void main(String[] args) {

        final Vector<String> tickets = new Vector<>();

        // Map<String, Object> map = Collections.synchronizedMap(new HashMap<String, Object>());

        for(int i=1;i<=1000;i++){
            tickets.add("火车票" + i);
        }

        for(int i=1;i<=10;i++){
            new Thread("线程"+i){
                @Override
                public void run() {
                    while (true){
                        if(tickets.isEmpty()){
                            break;
                        }
                        System.out.println(Thread.currentThread().getName()+"----"+tickets.remove(0));
                    }
                }
            }.start();
        }
    }

}
