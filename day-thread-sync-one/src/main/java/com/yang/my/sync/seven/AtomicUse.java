package com.yang.my.sync.seven;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/18 22:40
 * @Version 1.0
 */
public class AtomicUse {

    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public synchronized int add(){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        atomicInteger.addAndGet(1);
        atomicInteger.addAndGet(2);
        atomicInteger.addAndGet(3);
        atomicInteger.addAndGet(4);
        return atomicInteger.get();
    }


    public static void main(String[] args) {
        final AtomicUse atomicUse = new AtomicUse();
        List<Thread> threadList = new ArrayList<>();
        for(int i=0;i<100;i++){
            threadList.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(atomicUse.add());
                }
            }));
        }

        for(Thread thread:threadList){
            thread.start();
        }
    }

}
