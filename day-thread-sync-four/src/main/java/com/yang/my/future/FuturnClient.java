package com.yang.my.future;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:03
 * @Version 1.0
 */
public class FuturnClient {

    public Data request(final String queryStr){

        final FutureData futureData = new FutureData();

        new Thread(new Runnable() {
            @Override
            public void run() {
                RealData realData = new RealData(queryStr);
                futureData.setRealData(realData);
            }
        }).start();
        return futureData;
    }

}
