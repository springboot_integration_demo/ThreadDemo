package com.yang.my.coon.five;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:24
 * @Version 1.0
 */
public class Signletion {

    private static class InnerSingletion{
        private static Signletion signletion = new Signletion();
    }

    public static Signletion getInstance(){
        return InnerSingletion.signletion;
    }

}
