package com.yang.my.future;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 20:57
 * @Version 1.0
 */
public class RealData implements Data {

    private String msg;

    public RealData(String msg){
        System.out.println("根据:" + msg + "进行查询;这是一个很耗时的操作....");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("操作完毕;获取结果");
        this.msg = "洋洋洋的操作";
    }

    @Override
    public String getRequest() {
        return msg;
    }
}
