package com.yang.my.sync.five;

/**
 * synchronized重入
 * @Author: Mu_YI
 * @Date: 2019/3/17 21:06
 * @Version 1.0
 */
public class SyncReentryOne {

    private synchronized void methodOne(){
        System.out.println("methodOne....");
        methodTwo();
    }

    private synchronized void methodTwo(){
        System.out.println("methodTwo");
        methodThree();
    }

    private synchronized void methodThree(){
        System.out.println("methodThree");
    }

    public static void main(String[] args) {
        SyncReentryOne syncReentryOne = new SyncReentryOne();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                syncReentryOne.methodOne();
            }
        });
        thread.start();
    }

}
