package com.yang.my.coon.five;

import java.util.concurrent.TimeUnit;

/**
 * 同步代码块进行
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:26
 * @Version 1.0
 */
public class DoubleSingleton {

    private static DoubleSingleton ds;

    /**
     * Check double db
     * @return
     */
    private static DoubleSingleton getDs(){
        if(ds == null){
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (DoubleSingleton.class){
                if(ds == null ){
                    ds = new DoubleSingleton();
                }
            }
        }
        return ds;
    }

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t2");

        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(DoubleSingleton.getDs().hashCode());
            }
        },"t3");

        t1.start();
        t2.start();
        t3.start();

    }

}
