package com.yang.my.coon.one;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/18 23:09
 * @Version 1.0
 */
public class ListAddOne {

    private volatile static List list = new ArrayList();

    public void add() {
        list.add("hhhhh");
    }

    public int getSize() {
        return list.size();
    }

    public static void main(String[] args) {

        final ListAddOne listAddOne = new ListAddOne();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i <= 10; i++) {
                        listAddOne.add();
                        System.out.println("当前线程:" + Thread.currentThread().getName() + "添加了一个元素");
                        Thread.sleep(500);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (listAddOne.getSize() == 5) {
                        System.out.println("当前线程收到通知:" + Thread.currentThread().getName() + "list size =5 线程停止");
                        throw new RuntimeException();
                    }
                }
            }
        }, "t2");

        t1.start();
        t2.start();

    }

}
