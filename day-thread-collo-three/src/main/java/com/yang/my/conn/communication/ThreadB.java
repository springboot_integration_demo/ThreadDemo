package com.yang.my.conn.communication;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 16:27
 * @Version 1.0
 */
public class ThreadB extends Thread {

    private Object lock;

    public ThreadB(Object lock){
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock){
            System.out.println("释放开始时间:" + System.currentTimeMillis());
            lock.notify();
            System.out.println("释放结束时间:" + System.currentTimeMillis());
        }
    }
}
