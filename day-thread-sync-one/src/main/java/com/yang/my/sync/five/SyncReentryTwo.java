package com.yang.my.sync.five;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/17 21:11
 * @Version 1.0
 */
public class SyncReentryTwo {

    static class Main{
        public  int i = 10;
        public synchronized void operationSup(){
            try {
                i --;
                System.out.println("Main print i=" + i);
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class Sub extends Main{
        public synchronized void operationSub(){
            try {
                while (i>0){
                    i --;
                    System.out.println("Sub print i = " + i);
                    Thread.sleep(100);
                    this.operationSup();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Sub sub = new Sub();
                sub.operationSub();
            }
        });
        thread.start();
    }

}
