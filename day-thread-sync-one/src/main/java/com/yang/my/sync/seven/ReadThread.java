package com.yang.my.sync.seven;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/18 22:50
 * @Version 1.0
 */
public class ReadThread extends Thread {

    private volatile boolean isFlag = true;

    private void setIsFlag(boolean isFlag){
        this.isFlag = isFlag;
    }

    @Override
    public void run() {
        System.out.println("进入run方法");
        while (isFlag == true){
            // ...
        }
        System.out.println("线程停止");
    }

    public static void main(String[] args) {
        ReadThread readThread = new ReadThread();
        readThread.start();
        readThread.setIsFlag(false);
        System.out.println("isFlag被设置为true了。。。");
    }

}
