package com.yang.my.conn.communication;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 16:34
 * @Version 1.0
 */
public class ThreadLocalTest {

    private static ThreadLocal t1 = new ThreadLocal();

    public static void main(String[] args) {

        if(t1.get() == null){
            System.out.println("没有放值");
            t1.set("abc");
        }

        System.out.println(t1.get());
        System.out.println(t1.get());


    }

}
