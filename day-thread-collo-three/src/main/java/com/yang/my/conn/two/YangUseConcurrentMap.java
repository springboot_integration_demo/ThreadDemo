package com.yang.my.conn.two;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:45
 * @Version 1.0
 */
public class YangUseConcurrentMap {

    public static void main(String[] args) {
        ConcurrentMap<String,Object> concurrentMap = new ConcurrentHashMap<>();
        concurrentMap.put("k1","v1");
        concurrentMap.put("k2","v2");
        concurrentMap.put("k3","v3");
        concurrentMap.putIfAbsent("k4","vvvv");

    }

}
