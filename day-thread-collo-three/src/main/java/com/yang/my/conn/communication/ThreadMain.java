package com.yang.my.conn.communication;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 16:29
 * @Version 1.0
 */
public class ThreadMain {

    public static void main(String[] args) {
        Object lock = new Object();
        ThreadA threadA = new ThreadA(lock);
        threadA.start();

        ThreadB threadB = new ThreadB(lock);
        threadB.start();
    }

}
