package com.yang.my.future;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 20:57
 * @Version 1.0
 */
public interface Data {
    String getRequest();
}
