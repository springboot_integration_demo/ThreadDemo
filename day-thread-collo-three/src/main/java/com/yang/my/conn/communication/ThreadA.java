package com.yang.my.conn.communication;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 16:25
 * @Version 1.0
 */
public class ThreadA extends Thread{

    private Object lock;

    public ThreadA(Object lock){
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock){
            System.out.println("开始Lock  .." + System.currentTimeMillis());
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("结束Lock .." + System.currentTimeMillis());
        }
    }
}
