package com.yang.my.lambda.one;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *  简单的操作Lambda
 * @Author: Mu_YI
 * @Date: 2019/4/10 23:18
 * @Version 1.0
 */
public class LearnOneLambda {

    public static void main(String[] args) {
        // listIteration();
       // useStreamReduce();
        userStreamFilter();
    }

    public static void listIteration(){
        // List集合的遍历
        List features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        features.forEach(n -> System.out.println(n));

        // 线程启动
        new Thread(()->System.out.println("Lambda...Start Thread")).start();

        useStream();
    }

    private void learnArraySort(){
        Map<String,Integer> sortMapOne = new HashMap<>();
        Map<String,Integer> sortMapTwo = new HashMap<>();
        List<Map<String,Integer>> list = new ArrayList<>();
        Collections.sort(list,(m1,m2)-> m1.get("a").toString().compareTo(m2.get("a").toString()) );
    }

    public static void useStream(){
        List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
        costBeforeTax.stream().map((cost) -> cost + .12 * cost).forEach(System.out::println);
    }

    public static void useStreamReduce(){
        List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
        double total = costBeforeTax.stream().map((cost) -> cost + .12 * cost).reduce((sum,cost) -> sum + cost).get();
        System.out.println(total);
    }

    public static void userStreamFilter(){
        List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        List<String> newList = features.stream().filter(s -> s.contains("L")).collect(Collectors.toList());
        // features.stream().filter(s -> s.contains("L")).collect(Collectors);

        Map<String, String> stringMap = features.stream().collect(Collectors.toMap(s -> s, s -> s));
        System.out.println("MapInfo:" + stringMap);
        System.out.println(newList);
    }

}
