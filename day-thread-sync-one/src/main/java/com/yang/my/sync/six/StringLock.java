package com.yang.my.sync.six;

/**
 * synchronized代码块对字符串的锁,注意String常量池的缓存功能
 * @Author: Mu_YI
 * @Date: 2019/3/17 21:20
 * @Version 1.0
 */
public class StringLock {

    public void methodOne() {
        synchronized ("字符串得常量") {
            try {
                while (true) {
                    System.out.println("当前线程:" + Thread.currentThread().getName() + "开始");
                    Thread.sleep(1000);
                    System.out.println("当前线程:" + Thread.currentThread().getName() + "结束");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        StringLock stringLock = new StringLock();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                stringLock.methodOne();
            }
        }, "t1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                stringLock.methodOne();
            }
        }, "t2");
        t1.start();
        t2.start();

    }

}
