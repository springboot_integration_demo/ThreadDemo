package com.yang.my.conn.lock;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @Author: Mu_YI
 * @Date: 2019/4/11 16:58
 * @Version 1.0
 */

public class MyService {

    private Lock lock = new ReentrantLock();

    private Condition condition = lock.newCondition();

    private CountDownLatch countDownLatch = new CountDownLatch(3);

    public void test(){

        lock.lock();

        for(int i=0;i<=5;i++){
            System.out.println("当前线程:" + Thread.currentThread().getName() + " ( i" + i);
        }

        lock.unlock();
    }

}
