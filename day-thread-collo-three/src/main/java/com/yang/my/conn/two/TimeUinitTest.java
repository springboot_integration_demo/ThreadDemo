package com.yang.my.conn.two;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:43
 * @Version 1.0
 */
public class TimeUinitTest {

    private static TimeUnit timeUnit = TimeUnit.DAYS;

    public void outInfo(){
        System.out.println(timeUnit.name());
        System.out.println(timeUnit.toDays(1));
        System.out.println(timeUnit.toHours(1));
        System.out.println(timeUnit.toMinutes(1));
        System.out.println(timeUnit.toSeconds(1));
        System.out.println(timeUnit.toMillis(1));
        System.out.println(timeUnit.toMicros(1));
        System.out.println(timeUnit.toNanos(1));
        System.out.println((timeUnit.convert(1, TimeUnit.DAYS)) + timeUnit.name());
        System.out.println((timeUnit.convert(24, TimeUnit.HOURS)) + timeUnit.name());
        System.out.println((timeUnit.convert(1440, TimeUnit.MINUTES)) + timeUnit.name());
        System.out.println("-------------------");
    }

    public static void main(String[] args) {
        TimeUinitTest timeUinitTest = new TimeUinitTest();
        timeUinitTest.outInfo();
    }
}
