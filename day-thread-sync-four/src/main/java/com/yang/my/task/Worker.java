package com.yang.my.task;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:26
 * @Version 1.0
 */
public class Worker implements Runnable {

    private ConcurrentLinkedQueue<Task> workQueue;

    private ConcurrentHashMap<String,Object> resultMap;

    public void setWorkQueue(ConcurrentLinkedQueue<Task> workQueue){
        this.workQueue = workQueue;
    }

    public void setResultMap(ConcurrentHashMap<String,Object> resultMap){
        this.resultMap = resultMap;
    }


    private Object handler(Task input){
        Object output = null;
        try {
            TimeUnit.SECONDS.sleep(5);
            output = input.getPrice();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return output;
    }

    @Override
    public void run() {
        while (true){
            Task task = this.workQueue.poll();
            if(task == null){
                return;
            }
            Object handler = handler(task);
            this.resultMap.put(Integer.toString(task.getPrice()),handler);
        }
    }
}
