package com.yang.my.future;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/20 21:06
 * @Version 1.0
 */
public class AppMain {

    public static void main(String[] args) {
        FuturnClient futurnClient = new FuturnClient();
        Data data = futurnClient.request("请求参数");
        System.out.println("请求发送成功");
        System.out.println("做其他的事情");

        String request = data.getRequest();
        System.out.println(request);

    }

}
