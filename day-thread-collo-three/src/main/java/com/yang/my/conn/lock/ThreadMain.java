package com.yang.my.conn.lock;

/**
 * @Author: Mu_YI
 * @Date: 2019/4/11 17:02
 * @Version 1.0
 */
public class ThreadMain {

    public static void main(String[] args) {
        MyService myService = new MyService();
        MyThread m1 = new MyThread(myService);
        MyThread m2 = new MyThread(myService);
        MyThread m3 = new MyThread(myService);
        MyThread m4 = new MyThread(myService);
        MyThread m5 = new MyThread(myService);
        m1.start();
        m2.start();
        m3.start();
        m4.start();
        m5.start();
    }

}
