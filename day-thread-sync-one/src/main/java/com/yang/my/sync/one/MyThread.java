package com.yang.my.sync.one;

/**
 * 线程安全:当多个线程访问某一个类或者(对象或者方法时候),这个对象始终能保持正确的行为，这个方法就是安全的。
 * @Author: Mu_YI
 * @Date: 2019/3/17 20:30
 * @Version 1.0
 */
public class MyThread extends Thread {

    private int count = 5;

    @Override
    public synchronized void run() {
        count --;
        System.out.println(Thread.currentThread().getName()+": Count:" +count);
    }

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        Thread thread1 = new Thread(myThread,"t1");
        Thread thread2 = new Thread(myThread,"t2");
        Thread thread3 = new Thread(myThread,"t3");
        Thread thread4 = new Thread(myThread,"t4");
        Thread thread5 = new Thread(myThread,"t5");
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
    }
}
