package com.yang.my.coon.three;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Mu_YI
 * @Date: 2019/3/19 23:16
 * @Version 1.0
 */
public class ConnThreadLocal {

    private static ThreadLocal<String> th = new ThreadLocal<>();

    private void setTh(String value){
        th.set(value);
    }

    public void getTh(){
        System.out.println(Thread.currentThread().getName() + ":" + th.get());
    }

    public static void main(String[] args) {
        final ConnThreadLocal connThreadLocal = new ConnThreadLocal();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                connThreadLocal.setTh("张三");
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                connThreadLocal.getTh();
            }
        },"t1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    th.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t2");

        t1.start();
        t2.start();
    }

}
